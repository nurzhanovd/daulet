@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <h3>My tasks</h3>
            <a href="{{ route('tasks.create') }}" class="btn btn-success">Create</a>
            <div class="col-md-10 col-md-offset-1">
                <table class="table">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Title</td>
                        <td>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$task->id}}</td>
                            <td>{{$task->title}}</td>
                            <td>{{$task->description}}</td>
                            <td>
                                <a href="{{route('tasks.edit', $task->id)}}">
                                    <i class="glyphicon  glyphicon-edit"></i>
                                </a>
                                {!! Form::open(['method'=> 'DELETE',
                                                'route' => ['tasks.delete', $task->id]]) !!}
                                <button onclick="return confirm('are you sure?')">
                                    <i class="glyphicon  glyphicon-remove"></i>
                                </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection